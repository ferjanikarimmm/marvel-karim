import React from 'react';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

// import components:
import HeroImg from '../../components/HeroImg';
import HeroDetails from '../../components/HeroDetails';

import { useHeros } from './hooks';

const Hero = props => {
  const { getSingleHero, hero } = useHeros();

  React.useEffect(() => {
    const heroId = parseInt(props.match?.params.id);
    getSingleHero(heroId);
  }, []);

  return (
    <Grid container spacing={5}>
      <HeroImg thumbnail={hero.thumbnail} />
      <HeroDetails hero={hero} />
    </Grid>
  );
};

Hero.propTypes = {
  match: PropTypes.array,
};

export default Hero;
