import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../store';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

describe('pages | hero | index', () => {
  it('should render Hero', () => {
    const props = {
      match: {
        params: {
          id: 123,
        },
      },
    };
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <Router>
          <Component props={props} />
        </Router>
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
