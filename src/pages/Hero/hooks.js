import { useDispatch, useSelector } from 'react-redux';

import { getHero } from '../../selectors/herosSelector';
import { getSingleHero } from '../../ducks/heros';

export const useHeros = () => {
  const dispatch = useDispatch();
  return {
    hero: useSelector(getHero),
    getSingleHero: id => dispatch(getSingleHero(id)),
  };
};
