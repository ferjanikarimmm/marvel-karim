import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const useStyles = makeStyles({
  container: {
    textAlign: 'center',
    paddingLeft: '10%',
    paddingRight: '10%',
    paddingTop: 15,
  },
});

// import pages
import Heros from '../Heros';
import Hero from '../Hero';

const RouteProvider = () => {
  const classes = useStyles();

  return (
    <Grid className={classes.container}>
      <Router className={classes.container}>
        <Switch>
          <Route exact path="/" component={Heros} />
          <Route exact path="/:id" component={Hero} />
        </Switch>
      </Router>
    </Grid>
  );
};

export default RouteProvider;
