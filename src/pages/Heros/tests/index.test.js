import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../store';
import { Provider } from 'react-redux';

describe('pages | heros | index', () => {
  it('should render Heros', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <Component />
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
