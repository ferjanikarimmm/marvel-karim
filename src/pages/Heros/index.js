import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';

// import components
import HeroTitle from '../../components/HerosTitle';
import MarvelHerosContainer from '../../components/MarvelHerosContainer';

import { useHeros } from './hooks';

const Heros = () => {
  const { getAllHeros, herosList } = useHeros();

  useEffect(() => {
    getAllHeros();
  }, []);

  return (
    <Grid>
      <HeroTitle />
      <MarvelHerosContainer herosList={herosList} />
    </Grid>
  );
};

export default Heros;
