import { useDispatch, useSelector } from 'react-redux';

import { getAllHeros } from '../../ducks/heros';
import { getHerosList } from '../../selectors/herosSelector';

export const useHeros = () => {
  const dispatch = useDispatch();
  return {
    herosList: useSelector(getHerosList),
    getAllHeros: () => dispatch(getAllHeros()),
  };
};
