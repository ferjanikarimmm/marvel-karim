import { prop } from 'ramda';
import { createSelector } from 'reselect';

const root = prop('heros');

export const getHerosList = createSelector(root, prop('herosList'));
export const getHero = createSelector(root, prop('hero'));
