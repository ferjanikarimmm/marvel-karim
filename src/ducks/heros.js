import { getHerosListFromApi, getSingleHeroFromApi } from '../services/herosService';

export const GET_HEROS = 'GET_HEROS';
export const GET_HERO = 'GET_HERO';

export const getAllHeros = () => dispatch => {
  getHerosListFromApi().then(res => {
    dispatch({
      type: GET_HEROS,
      herosList: res.data.results,
    });
  });
};

export const getSingleHero = id => dispatch => {
  getSingleHeroFromApi(id).then(res => {
    dispatch({
      type: GET_HERO,
      hero: res.data.results[0],
    });
  });
};

const actions = { getAllHeros, getSingleHero };

const initState = {
  herosList: [],
  hero: {},
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case GET_HEROS:
      return {
        ...state,
        herosList: action.herosList,
      };
    case GET_HERO:
      return {
        ...state,
        hero: action.hero,
      };
    default:
      return state;
  }
};

export default { actions, reducer };
