import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configureStore from './store';
const store = configureStore();

import RouteProvider from './pages/RouteProvider';

const ROOT = (
  <Provider store={store}>
    <RouteProvider />
  </Provider>
);

ReactDOM.render(ROOT, document.getElementById('root'));
