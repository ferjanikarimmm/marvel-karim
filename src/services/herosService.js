const axios = require('axios');
var crypto = require('crypto');
import { prop } from 'ramda';

const baseUrl = 'http://gateway.marvel.com:80';

const uri = '/v1/public/characters';
export const charactersUrl = baseUrl + uri;

const timestamp = [Math.round(+new Date() / 1000)];

const privateApi = '2fc675e7a9219a4cdad260047213bb4d1ca78649';
const publicApi = 'e36bcf87289cb659ff3f1a93b7e279b3';

const concatenatedString = timestamp.concat(privateApi, publicApi).join('');

const hash = crypto.createHash('md5').update(`${concatenatedString}`).digest('hex');

export const getHerosListFromApi = () =>
  axios
    .get(charactersUrl, {
      params: {
        ts: timestamp,
        apikey: publicApi,
        hash,
      },
    })
    .then(prop('data'));

export const getSingleHeroFromApi = id =>
  axios
    .get(charactersUrl + '/' + id, {
      params: {
        ts: timestamp,
        apikey: publicApi,
        hash,
      },
    })
    .then(prop('data'));
