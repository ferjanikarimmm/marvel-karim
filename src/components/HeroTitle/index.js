import React from 'react';
import { makeStyles, Paper, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  container: {
    padding: 15,
    paddingLeft: 30,
    textAlign: 'left',
  },
  subTitle: {
    opacity: '0.7',
    fontSize: 14,
  },
});

const HeroTitle = ({ name, description }) => {
  const classes = useStyles();

  return (
    <Paper elevation={3} className={classes.container}>
      <Typography variant="h5" gutterBottom>
        {name}
      </Typography>

      <Typography variant="subtitle1" gutterBottom className={classes.subTitle}>
        {description}
      </Typography>
    </Paper>
  );
};

HeroTitle.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
};

export default HeroTitle;
