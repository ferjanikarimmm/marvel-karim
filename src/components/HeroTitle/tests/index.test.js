import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe.only('components | HeroTitle | index', () => {
  it('should render component', () => {
    const name = '';
    const description = '';
    const { container } = render(<Component name={name} description={description} />);
    expect(container).toMatchSnapshot();
  });
});
