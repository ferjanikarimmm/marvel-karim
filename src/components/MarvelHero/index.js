import React from 'react';
import { Grid, Typography, Paper, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  container: {
    padding: 20,
    backgroundColor: '#00000007',
  },
  img: {
    height: 250,
    width: '90%',
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  infoContainer: {},
  heroTitle: {
    marginBottom: 20,
  },
  link: {
    width: '30%',
    textDecoration: 'none',
    color: '#00000080',
  },
});

const MarvelHero = ({ item }) => {
  const classes = useStyles();

  return (
    <Grid item md={4} xs={12}>
      <Paper elevation={3} className={classes.container}>
        <img src={item.thumbnail.path + '.' + item.thumbnail.extension} alt="" className={classes.img} />

        <Grid className={classes.infoContainer}>
          <Link to={`/${item.id}`}>
            <Typography variant="h6" gutterBottom className={classes.heroTitle}>
              {item.name}
            </Typography>
          </Link>

          <Grid container>
            {item.urls.map((link, key) => (
              <a key={key} rel="noreferrer" className={classes.link} target="_blank" item href={link.url}>
                {link.type}
              </a>
            ))}
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
};

MarvelHero.propTypes = {
  item: PropTypes.object,
};

export default MarvelHero;
