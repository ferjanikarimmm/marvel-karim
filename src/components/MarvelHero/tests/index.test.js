import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';

import Component from '..';

describe.only('components | MArvelHero | index', () => {
  it('should render component', () => {
    const item = {
      urls: [{ type: '', url: '' }],
      thumbnail: {
        path: '',
        extension: '',
        resourceURI: '',
      },
    };
    const { container } = render(
      <Router>
        <Component item={item} />
      </Router>,
    );
    expect(container).toMatchSnapshot();
  });
});
