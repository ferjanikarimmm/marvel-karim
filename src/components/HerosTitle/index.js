import React from 'react';
import { Grid, Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  titleContainer: {
    textAlign: 'center',
  },
  title: {},
});

const HerosTitle = () => {
  const classes = useStyles();

  return (
    <Grid className={classes.titleContainer}>
      <Typography variant="h4" gutterBottom className={classes.title}>
        Liste des super héros :
      </Typography>
    </Grid>
  );
};

export default HerosTitle;
