import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe.only('components | HeroMembershipList | index', () => {
  it('should render component', () => {
    const items = { items: [] };
    const title = '';
    const { container } = render(<Component items={items} title={title} />);
    expect(container).toMatchSnapshot();
  });
});
