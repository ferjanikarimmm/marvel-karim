import React from 'react';
import { Grid, makeStyles, Typography, ListItemText, ListItem, List, Box } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  title: {
    textAlign: 'left',
    marginTop: 25,
    marginLeft: 15,
  },
  listItem: {},
  listItemText: {
    fontSize: '10px!important',
    opacity: 0.7,
    borderBottomWidth: 1,
  },
}));

const HeroMembershipList = ({ items, title }) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={6}>
      <Typography className={classes.title} variant="subtitle2" display="block" gutterBottom>
        {title}
      </Typography>
      <Grid>
        <List>
          {items?.items.map((item, key) => (
            <Box borderBottom={1} key={key}>
              <ListItem className={classes.listItem}>
                <ListItemText className={classes.listItemText} primary={item.name} />
              </ListItem>
            </Box>
          ))}
        </List>
      </Grid>
    </Grid>
  );
};

HeroMembershipList.propTypes = {
  items: PropTypes.object,
  title: PropTypes.string,
};

export default HeroMembershipList;
