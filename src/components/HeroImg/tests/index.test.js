import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Component from '..';

describe.only('components | HeroImg | index', () => {
  it('should render component', () => {
    const thumbnail = {};
    const { container } = render(
      <Router>
        <Component thumbnail={thumbnail} />
      </Router>,
    );
    expect(container).toMatchSnapshot();
  });
});
