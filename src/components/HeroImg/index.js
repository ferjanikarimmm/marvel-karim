import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  container: {},
  img: {
    width: '100%',
  },
  linkContainer: {
    marginBottom: 20,
  },
});

const Hero = ({ thumbnail }) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} md={3} className={classes.container}>
      <Grid className={classes.linkContainer}>
        <Grid component={Link} item xs={12} to="/">
          Go Back
        </Grid>
      </Grid>

      {thumbnail?.path !== undefined ? (
        <img src={thumbnail.path + '.' + thumbnail.extension} alt="" className={classes.img} />
      ) : (
        <img src="" alt="" className={classes.img} />
      )}
    </Grid>
  );
};

Hero.propTypes = {
  thumbnail: PropTypes.object,
};

export default Hero;
