import React from 'react';
import { Grid, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  container: {
    marginTop: 20,
  },
});

// import component
import MarvelHero from '../MarvelHero';

const MarvelHerosContainer = ({ herosList }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={6} className={classes.container}>
      {herosList.map((item, key) => (
        <MarvelHero key={key} item={item} />
      ))}
    </Grid>
  );
};

MarvelHerosContainer.propTypes = {
  herosList: PropTypes.array,
};

export default MarvelHerosContainer;
