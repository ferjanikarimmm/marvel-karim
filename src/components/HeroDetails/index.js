import React from 'react';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

// import components:
import HeroTitle from '../HeroTitle';
import HeroMembershipList from '../HeroMembershipList';

const HeroDetails = ({ hero }) => {
  return (
    <Grid item xs={12} md={9}>
      <HeroTitle name={hero.name} description={hero.description} />
      <Grid container spacing={2}>
        <HeroMembershipList items={hero.comics} title="Comics" />
        <HeroMembershipList items={hero.series} title="series" />
      </Grid>
    </Grid>
  );
};

HeroDetails.propTypes = {
  hero: PropTypes.object,
};

export default HeroDetails;
